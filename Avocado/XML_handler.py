#!/usr/bin/env python

import xml.etree.ElementTree as etree

class XML:
    def __init__(self, path):
        self.path = path
        self.xml = etree.parse(self.path)
        self.root = self.xml.getroot()
        self.dictionary = self.generate_dictionary()

    def return_dictionary(self):
        return self.dictionary

    def generate_dictionary(self):
        return self.parse_tree(self.root)

    def query(self, entry):
        return self.find_in_dictionary(entry, self.dictionary)

    def find_in_dictionary(self, entry, dictionary):

        #check if the entry is in the dictionary main level
        if entry in dictionary: 
            return dictionary[entry]

        else:
            #iterate through all the values in the dictionary and return the value with the keywords
            for k, v in dictionary.items():
                #check if our value is also a dictionary (if not, we do not want to call recursively on it)            
                if isinstance(v, dict):
                    return self.find_in_dictionary(entry, v)      

    def parse_tree(self, tree):
        
        #create a dictionary of the current tree tag IF it has attributes
        dmain = {tree.tag : {} if tree.attrib else None}

        #find out how many/if any branches there are off this tree
        branches = [branch for branch in tree]

        if branches:

            #dictionary to store the recursive branches
            cd = {}

            for branch in branches:
                #for each branch recursively call this function until we hit the end of the tree
                next_branch = self.parse_tree(branch)

                #working recursively, get the value/tag of the branches and what they are
                for key, val in next_branch.items():
                    if key in cd:
                        cd[key].append(val)
                    else:
                        cd[key] = [val]

            #once we have recursively parsed the tree, populate the dmain dictionary with the cd dictionary values
            dmain = {tree.tag: {k:v if len(v) > 1 else v[0] for k, v in cd.items()}}

        #for each branch, check if we have any attributes or text, and populate the dictionary IF it exists, otherwise create a new entry
        if tree.attrib:
            #create an attribute tag (@) and add all the relevant items to it
            dmain[tree.tag].update(('@' + k, v) for k, v in tree.attrib.items())
        if tree.text:
            text = tree.text.strip()
            #if we have an attribute or branches AND there is a text entry in the XML, create a (#text) entry
            if branches or tree.attrib:
                if text:
                    dmain[tree.tag]['#text'] = text
            else:
                dmain[tree.tag] = text

        return dmain
