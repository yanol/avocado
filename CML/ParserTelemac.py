#!/usr/bin/env python

"""
    This file is part of Avocado.

    Avocado is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Avocado is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Avocado.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import os
import tkMessageBox
dic_path = os.path.abspath(os.path.join(__file__ ,"../..",'Dictionaries','telemac2d.dico'))


class Parser():
    def __init__(self,path=None, dic_path = dic_path):
        if path:
            data = readFile(path)
            self.data = self.format_cas(data)
        dic_data = readFile(dic_path)
        self.dic = self.make_dic(dic_data)

        
    def make_dic(self,data):
        '''Segment the data into a list with each entry represening a keyword and then segment further into a dictionary with each keyword correspond to a key mapping to another dictionary with all the other parameters (default value help etc)'''
        data = segment_data(data)
        data = delete_whitespace(data)
        data = join_lines(data)
        data = organize_fields(data)
        dic =  self.create_dic(data)
        return dic

    def create_dic(self, data):
        '''create a python dicionary from the telemac dictionary file'''
        dic = {}
        for item in data:
            key = findKeyword('NOM1','.+\s*[:=]\s*(.+)\s*', data= item)
            #print('original',key)
            #remove the quotation marks    
            print(key)
            if key:    
            #the if-key is not optimal and should be replaced it is just that there is an empty instance in the beginning that ruins everything otherwise
                key = regexSearch('',[key],r'''^\s*["']*(.*?)["']*\s*$''')[0]

                dic[key] = {}
                entries = regexSearch('',item, '(.+)\s*[:=]\s*.+\s*')
                entries = [regexSearch('',[entry],r'''^\s*["']*(.*?)["']*\s*$''')[0] for entry in entries ]
                results = regexSearch('',item, '.+\s*[:=]\s*(.+)\s*')

                results = [regexSearch('',[result], r'''^\s*["']*(.*?)["']*\s*$''') for result in results]
                for result,entry in zip(results,entries):
                    dic[key][entry]=result[0]
            else:
                print('Could not find kind')
        return dic

    def format_cas(self, data):
        '''remove empty lines and comments from the cas file'''
        data = segment_data(data)
        data = delete_whitespace(data)
        return data


    def search_keywords(self, fields= [None], data=None):
        '''search for the values of the parameters of the simulation fields (ex: ['TIDAL FLATS', 'WIND SPEED']) is a list of the fields to be parsed for. Return a list with the values the parameters were set too. If a parameter is not in the cas file and does not have a default value specified in the dictionnary the value None is returned for that parameter '''
        #keyword dic is a dictionary with the fields specified
        if not data:
            data = self.data
        keyword_dic ={}
        for keyword in fields:
            if keyword in self.dic:
                entry = regexSearch(keyword, data, '\s*[:=]\s*(.*)')

                if entry:
                    entry = regexSearch('', [entry[0]], r'''^\s*["']*(.*?)["']*\s*$''')
                    print(entry)
                    keyword_dic[keyword] = entry[0]
                else:
                    #try keyword in french
                    #could give error if nom category is not created (potential bug)
                    entry =regexSearch(self.dic[keyword]['NOM'], data, '\s*[:=]\s*(.*)')

                    if entry:
                        entry = regexSearch('', [entry[0]], r'''^\s*["']*(.*?)["']*\s*$''')
                        print(entry)
                        keyword_dic[keyword] = entry[0]
                    else:

                        if 'DEFAUT1' in self.dic[keyword]:
                            keyword_dic[keyword] = self.dic[keyword]['DEFAUT1']
                        else:
                            keyword_dic[keyword] = None
                        
            else:
                raise KeyError('One of your keys is not in the telemac dictionary')

        return keyword_dic
                    

        
        
        
def readFile(path=dic_path):
    '''open the file found at filepath given by path and return a list of all the lines'''
    #print('reading file')
    try:
        with open(path) as f:
            data = f.readlines()
        data= [datum.rstrip('\n') for datum in data]
        return data
    except IOError:
        tkMessageBox.showerror('Error', 'Something went wrong in opening' + path)
    


def regexSearch(text, data, regex='\s*[:=]\s*(.*)'):
    '''search using regular expressions for matches in the data by default the regexsearch parses the data input and matches what is after the equal sign for the specified text input'''
    results =[]
    pattern = text + regex
    #print(pattern)
    p = re.compile(pattern)
    for line in data:
        m = p.match(line)
        if m:    
            #print(m.group(1))
            results.append( m.group(1) )
    return results

def map_function(line):
    '''function used by the map statement in segment_data function'''
    if re.match('^\s*\/',line):
        return None
    elif re.match('^\s*$',line):
        return None
    else: 
        return line


def segment_data(data):
    '''replace all full line comments and all empty lines from the data with the None element'''
    filtered_data = map(map_function, data)
    return filtered_data

def delete_whitespace(data):
    '''removes all none from the data corresponding to removing comments and whitespace lines'''
    i=0
    while i < len(data):
        if not data[i]:
            del data[i]
        else:
            i=i+1
    return data


def organize_fields(data):
    '''segment data in such a way that each new keyword corresponds to a new sublist'''
    nested_list=[[]]
    i=0
    for line in data:
        if re.match('^\s*NOM\s*=',line):
            nested_list.append([line])
        else:
            nested_list[-1].append(line)
        i=+1
        
    return nested_list

def join_lines(data):
    '''join lines where the data took over multiple lines.'''
    nested_list=['']
    i=0
    for i in range(len(data)):
        if  not re.match('^\s*[A-Z1-9]+\s*=',data[i]):
            #print('joining', data[i])
            nested_list[-1]=nested_list[-1]+data[i]
        else:
            nested_list.append(data[i])
    return nested_list


##removeInlineComments is still a work in progress
def removeInlineComments(data):
    ''' removes comments between // while checking that they are not found between '' (such as found in filepaths)'''
    new_data = []
    for datum in data:
        print(datum)
        #check if there is a / between two ''. If there isn't then we assume that // means that it is a comment and that we can remove it''
        #As it stands the program only removes a maximum of one comment per line if there is a '/ 'then it will not remove the comments
        print('trying match')
        if not re.match(r"[^\/']+'[^\/']*\/[^']*'", datum):
            print('didnt get match')
            comment = regexSearch('',[datum], r"[^\/]*(\/[^\/]*\/)[^\/']*")
            if comment:
                print 'there is a comment %s', comment[0]
                datum = datum.replace(comment[0],'')
        new_data.append(datum)
    return new_data


def removeEndOfLineComments(data):
    #As it is know this does not remove the end of line comments if the line contains some / this can be easily solved so long as this function is called after removeinlinecomments
    new_data = []
    for datum in data:
        result = regexSearch('',[datum],r"[^\/]*(/[^\/']*)$")
        if result:
            datum = datum.replace(result[0],'')
        new_data.append(datum)
    return new_data


def findKeyword(field, regex, data=None,path=None):
    #Function takes path_steering as an input the field being looked for and the regex pattern to match
    if not data:
        data = readFile(path)
    #data=removeComments(data)
    result = regexSearch(field,data, regex)
    if result:
        return result[0]
    else:
        return None

def getFilePaths(path=dic_path):
    '''getFilePaths finds the input and output file names for the specific simulation
    the inputs are the path towards the cas file.'''
    data = readFile(path)
    #^\s*[:=]\s*(.*) matches the text after an equal or : sign
    sel_path = findKeyword('GEOMETRY FILE', '\s*[:=]\s*(.*)', data= data)
    cli_path = findKeyword('BOUNDARY CONDITIONS FILE', '\s*[:=]\s*(.*)',data= data)
    res_path = findKeyword('RESULTS FILE', '\s*[:=]\s*(.*)',data= data)
    #remove '' and ./ to only keep file name
    print('sel_path is', sel_path)
    sel_path = regexSearch('',[sel_path],'''^[\s'".\/]*([^\s'"\/]+)[\s'"]*''')[0]
    cli_path = regexSearch('',[cli_path],'''^[\s'".\/]*([^\s'"\/]+)[\s'"]*''')[0]
    res_path = regexSearch('',[res_path],'''^[\s'".\/]*([^\s'"\/]+)[\s'"]*''')[0]
    
    '''This string can be used to only keep the file name ^[\s'".\/]*([^\s'"\/]+)[\s'"]*'''
    #print(sel_path, cli_path, res_path)
    return sel_path, cli_path, res_path




#Possible issues
#comments inline // and the possibility that they are in string '//' ex pathfiles
#Grouping default variables to their respective options (sometimes have default othertimes have choice othertimes don't have choice)
#Knowing what type of variable we are expecting
#If the user puts a variable twice(once and then reassigns)
#Start and end tags
#Putting the same tag twice???

